import http from 'node:http';
import https from 'node:https';

import buildURL from 'axios/unsafe/helpers/buildURL.js';
import buildFullPath from 'axios/unsafe/core/buildFullPath.js';
import settle from 'axios/unsafe/core/settle.js';

export default config => new Promise((resolve, reject) => {
    const
        url = buildURL(buildFullPath(config.baseURL, config.url), config.params, config.paramsSerializer),
        parsedUrl = new URL(url),
        { protocol } = parsedUrl,
        {
            username,
            password
        } = config.auth || parsedUrl,
        request = ({
            'http:': http,
            'https:': https
        })[protocol].request(
            url,
            {
                method: config.method,
                headers: config.headers,
                agent: {
                    'http:': config.httpAgent,
                    'https:': config.httpsAgent
                }[protocol],
                auth: username || password ? `${username || ''}:${password || ''}` : undefined
            }
        );
    request.on('error', reject);
    request.write(config.data);
    request.end(() => settle(
        resolve,
        reject,
        {
            config,
            request
        }
    ));
});